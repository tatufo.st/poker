import { shuffle } from "../Actions/Shuffle";
import { deal } from "../Actions/Deal";
import { rate } from "../Actions/Rate";
import { refresh } from "../Actions/Refresh";
import { select } from "../Actions/Select";
import { trade } from "../Actions/Trade";
import {
  combinations,
  maxValueIndex,
  PokerHandRate,
  RateableCards,
  startingDeck,
  CardsAndDeck,
  tradeCard
} from "../../lib/pokerHelperFunctions";
const game = (
  state = {
    Deck: CardsAndDeck(startingDeck).deck,
    players: [
      {
        nameOfPlayer: "player1",
        cards: [],
        combination: ""
      },
      {
        nameOfPlayer: "player2",
        cards: [],
        combination: ""
      }
    ],
    round: 0
  },
  action
) => {
  switch (action.type) {
    case shuffle:
      return { ...state, Deck: action.payload, round: 1 };
    case deal:
      let newDeck = state?.Deck || startingDeck;
      const newPlayers = state?.players.map((player) => {
        const { cards, deck } = CardsAndDeck(newDeck, 5);
        newDeck = deck;
        return { ...player, cards };
      });
      return {
        ...state,
        players: newPlayers,
        Deck: newDeck,
        round: 2
      };
    case rate:
      const cardsCombination = state?.players.map((player) => {
        const combination = PokerHandRate(new RateableCards(player.cards));
        const result = maxValueIndex(
          combinations,
          PokerHandRate(new RateableCards(player.cards))
        );
        return { ...player, combination, result };
      });
      return {
        ...state,
        players: cardsCombination,
        round: 4
      };
    case refresh:
      state = {
        Deck: CardsAndDeck(startingDeck).deck,
        players: [
          { nameOfPlayer: "player1", cards: [], combination: "" },
          { nameOfPlayer: "player2", cards: [], combination: "" }
        ],
        round: 0
      };
      return state;
    case select:
      return {
        ...state,
        players: state.players.map((player) =>
          player.nameOfPlayer === action.payload.playerName
            ? {
                ...player,
                cards: player.cards.map((card, index) =>
                  index === action.payload.index
                    ? { ...card, isSelected: action.payload.isSelected }
                    : card
                )
              }
            : player
        )
      };
    case trade:
      let afterTradeDeck = state.Deck;
      const updatedPlayers = state.players.map((player) => {
        const updatedCards = player.cards.map((card) => {
          if (card.isSelected === true) {
            const { tradedCard, deck } = tradeCard(afterTradeDeck, 1);
            afterTradeDeck = deck;
            return tradedCard;
          } else {
            return card;
          }
        });
        return { ...player, cards: updatedCards };
      });
      return {
        ...state,
        players: updatedPlayers,
        Deck: afterTradeDeck,
        round: 3
      };
    default:
      return state;
  }
};

export default game;
