export const shuffle = "SHUFFLE";
const Shuffle = (payload) => ({
  type: "SHUFFLE",
  payload: payload
});
export default Shuffle;
