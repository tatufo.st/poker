import "./App.css";
import { connect } from "react-redux";
import { Shuffle, Deal, Rate, Refresh, Select, Trade } from "./Models";
import {
  Button,
  RatePlayer,
  WinnerDecider,
  RefreshGame,
  Hand
} from "./Components";

const App = ({
  handleClickDeck,
  handleClickRate,
  handleClickDeal,
  handleClickRefresh,
  handleClickSelect,
  handleClickTrade,
  state
}) => {
  const deckLength = state?.game?.Deck.length;

  return (
    <div className="body">
      <div className="board" />

      <Button
        handleClick={() => {
          handleClickDeck(state.game.Deck);
        }}
        disabled={state.game.round === 0 ? false : true}
        className={state.game.round === 0 ? "btnDeck" : "btnDeckDisabled"}
      >
        Shuffle
      </Button>

      <Button
        handleClick={() => handleClickDeal({})}
        disabled={state.game.round === 1 ? false : true}
        className={state.game.round === 1 ? "btnDeal" : "btnDealDisabled"}
      >
        Deal
      </Button>

      <Button
        handleClick={() => handleClickTrade({})}
        disabled={state.game.round === 2 ? false : true}
        className={state.game.round === 2 ? "btnTrade" : "btnTradeDisabled"}
      >
        Trade Cards
      </Button>

      <Button
        handleClick={() => handleClickRate({})}
        disabled={state.game.round === 3 ? false : true}
        className={state.game.round === 3 ? "btnRate" : "btnRateDisabled"}
      >
        Rate
      </Button>

      <Button
        handleClick={() => handleClickRefresh({})}
        disabled={state.game.round === 4 ? false : true}
        className={state.game.round === 4 ? "btnNewGame" : "btnNewGameDisabled"}
      >
        Refresh
      </Button>

      {deckLength < 10 ? (
        <RefreshGame handleClick={() => handleClickRefresh({})} />
      ) : (
        state?.game?.players.map((statePlayer, index) => {
          return (
            <div key={index}>
              <Hand
                player={statePlayer}
                handleClickSelect={handleClickSelect}
              />
            </div>
          );
        })
      )}
      {state?.game?.players[0]?.result >= 0 && deckLength >= 10
        ? state?.game?.players.map((statePlayer, index) => {
            return (
              <div
                key={index}
                className={
                  statePlayer.nameOfPlayer === "player1"
                    ? "ratePlayer1"
                    : "ratePlayer2"
                }
              >
                <RatePlayer player={statePlayer} />
              </div>
            );
          })
        : null}
      {state?.game?.players[0]?.combination !== "" && deckLength >= 10 ? (
        <div className="winner">
          <WinnerDecider state={state} />
        </div>
      ) : null}
    </div>
  );
};
const mapStateToProps = (state) => {
  return { state };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleClickDeck: (payload) => {
      dispatch(Shuffle(payload));
    },
    handleClickDeal: (payload) => {
      dispatch(Deal(payload));
    },
    handleClickRate: (payload) => {
      dispatch(Rate(payload));
    },
    handleClickRefresh: () => {
      dispatch(Refresh());
    },
    handleClickSelect: (isSelected, index, playerName) => {
      dispatch(Select({ isSelected, index, playerName }));
    },
    handleClickTrade: (payload) => {
      dispatch(Trade(payload));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

//css double classname
