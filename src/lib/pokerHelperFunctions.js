import _ from "lodash";

// function deepFreeze(object) {
//   if (typeof object !== "object") {
//     return object;
//   }
//   Object.freeze(object);

//   Object.values(object).forEach((value) => deepFreeze(value));

//   return object;
// }

const maxInARow = (weights) =>
  _.chain(weights)
    .sortBy()
    .uniq()
    .map((num, i) => num - i)
    .groupBy()
    .orderBy("length")
    .last()
    .value().length;

// if (valueOfMax > element)
export const maxValueIndex = (arr, valueOfMax) => {
  return arr.reduce((acc, element, index) => {
    if (valueOfMax === element) {
      acc = index;
      valueOfMax = element;
    }
    return acc;
  }, 0);
};

export class RateableCards {
  constructor(cards) {
    this.ranks = _.groupBy(cards, "rank");
    this.suits = _.groupBy(cards, "suit");
    this.rankTimes = _.groupBy(this.ranks, "length");
    this.suitTimes = _.groupBy(this.suits, "length");
    this.maxInARow = maxInARow(cards.map(({ weight }) => weight));
  }

  getOfSameRank(n) {
    return this.rankTimes[n] || [];
  }

  getOfSameSuit(n) {
    return this.suitTimes[n] || [];
  }

  hasAce() {
    return !!this.ranks["A"];
  }

  hasOfSameRank(n) {
    return this.getOfSameRank(n).length;
  }

  hasOfSameSuit(n) {
    return this.getOfSameSuit(n).length;
  }

  hasInARow(n) {
    return this.maxInARow >= n;
  }

  getWorstSingles() {
    return _.chain(this.getOfSameRank(1)).flatten().sortBy("weight").value();
  }
}

// Poker Ratings used to determine combination on the hand from PokerHandRate below
export const PokerRating = {
  RoyalFlush: (hand) =>
    hand.hasInARow(5) && hand.hasOfSameSuit(5) && hand.hasAce(),
  StraightFlush: (hand) => hand.hasInARow(5) && hand.hasOfSameSuit(5),
  FourOfAKind: (hand) => hand.hasOfSameRank(4),
  FullHouse: (hand) => hand.hasOfSameRank(3) && hand.hasOfSameRank(2),
  Flush: (hand) => hand.hasOfSameSuit(5),
  Straight: (hand) => hand.hasInARow(5),
  ThreeOfAKind: (hand) => hand.hasOfSameRank(3),
  TwoPair: (hand) => hand.hasOfSameRank(2) >= 2,
  OnePair: (hand) => hand.hasOfSameRank(2),
  HighCard: (hand) => hand.hasOfSameRank(1) >= 5
};

const Ranks = [
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K",
  "A"
];
const isSelected = false;
const Suits = ["hearts", "clubs", "diams", "spades"];

// starting deck to use for the game. Creates 52 cards with rank, suit and weight
// example: [{
// "rank": "2",
// "suit": "hearts",
// "weight": "0"
// },
// ...
// {
//   "rank": "A",
//   "suit": "spades",
//   "weight": "12"
// }]
export const startingDeck = Object.entries(Ranks).reduce(
  (cards, [weight, rank]) => [
    ...cards,
    ...Suits.map((suit) => ({ rank, suit, weight, isSelected }))
  ],
  []
);

// function that gets as parameters the deck if it exists and cards have been taken from it
// and the number of cards that the user wants to take from the deck
// returns an object with the cards that the user will get from the deck and the remaining deck
// to use in the game

export const shuffleDeck = (deck) => {
  const shuffledDeck = deck.slice(0, 53).sort(() => Math.random() - 0.5);
  return shuffledDeck;
};

export const dealShuffledDeck = ({ shuffledDeck }) => {
  return true;
};

export const CardsAndDeck = (currentDeck = startingDeck, n = 0) => {
  const deck =
    currentDeck !== startingDeck
      ? currentDeck.slice(n, currentDeck.length)
      : currentDeck
          .sort(() => Math.random() - 0.5)
          .slice(n, currentDeck.length);

  const cards1 = currentDeck.slice(0, n);
  const cards = cards1.map((card) => {
    const cardToReturn = { ...card, weight: parseInt(card.weight) };
    return cardToReturn;
  });
  return {
    cards,
    deck
  };
};

export const tradeCard = (currentDeck, n = 0) => {
  const deck =
    currentDeck !== startingDeck
      ? currentDeck.slice(n + 1, currentDeck.length)
      : null;

  const tradedCard = currentDeck[n];

  return {
    tradedCard,
    deck
  };
};

// possible combinations
export const combinations = [
  "RoyalFlush",
  "StraightFlush",
  "FourOfAKind",
  "FullHouse",
  "Flush",
  "Straight",
  "ThreeOfAKind",
  "TwoPair",
  "OnePair",
  "HighCard"
];

// players array for the game (host is the the application and player is the user)
export const players = ["host", "player"];

export const PokerHandRate = (cards) =>
  Object.entries(PokerRating).find(([, is]) => is(cards))[0];

// function to get the best hand. usage:
// const combination = PokerHandRate(new RateableCards(hand))
// where hand could be for example:
// [
//   {
//     "rank": "2",
//     "suit": "hearts",
//     "weight": 0
// },
// {
//     "rank": "2",
//     "suit": "clubs",
//     "weight": 0
// },
// {
//     "rank": "2",
//     "suit": "diams",
//     "weight": 0
// },
// {
//     "rank": "2",
//     "suit": "spades",
//     "weight": 0
// },
// {
//     "rank": "3",
//     "suit": "hearts",
//     "weight": 1
// }
// ]

// test

//redux
