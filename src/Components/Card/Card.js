const Card = ({ player, handleClickSelect }) => {
  const handleClick = (index, playerName) => {
    const isSelected = player.cards[index].isSelected;
    handleClickSelect(!isSelected, index, playerName);
  };

  return player.cards.length !== 0 ? (
    player.cards.map((card, index) => {
      const className = () => {
        if (player.nameOfPlayer === "player1") {
          return `cardPlayerOne${card.isSelected ? "Selected" : ""}`;
        } else if (player.nameOfPlayer === "player2") {
          return `cardPlayerTwo${card.isSelected ? "Selected" : ""}`;
        }
      };

      return (
        <div
          key={index}
          className={className()}
          onClick={() => handleClick(index, player.nameOfPlayer)}
        >
          Rank: {card.rank}
          <br />
          Suit: {card.suit}
          <br />
          Weight: {card.weight}
        </div>
      );
    })
  ) : (
    <div
      className={player.nameOfPlayer === "player1" ? "playerOne" : "playerTwo"}
    >
      {player.nameOfPlayer}
    </div>
  );
};

export default Card;
