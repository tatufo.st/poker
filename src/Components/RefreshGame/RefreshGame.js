const RefreshGame = ({ handleClick }) => {
  return (
    <div>
      <div className="noCards">No more cards, refresh ?</div>
      <button className="refreshButton" onClick={handleClick}>
        Refresh
      </button>
    </div>
  );
};

export default RefreshGame;
