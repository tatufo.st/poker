const RatePlayer = ({ player }) => {
  return player.cards.combination !== "" ? (
    <div>{player.combination}</div>
  ) : (
    <div
      className={
        player.nameOfPlayer === "player1" ? "ratePlayer1" : "ratePlayer2"
      }
    >
      {player.nameOfPlayer}
    </div>
  );
};

export default RatePlayer;
