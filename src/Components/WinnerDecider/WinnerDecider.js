const WinnerDecider = ({ state }) => {
  const player1 = state.game.players[0].result;
  const player2 = state.game.players[1].result;
  const compare =
    player1 === player2
      ? "Draw! Look for highest Card"
      : player1 < player2
      ? "Blue Player Wins"
      : "Red Player Wins";
  return compare;
};

export default WinnerDecider;
