const Button = ({ children, className, handleClick, disabled }) => {
  return (
    <button onClick={handleClick} className={className} disabled={disabled}>
      {children}
    </button>
  );
};

export default Button;
