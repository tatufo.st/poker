import Card from "../Card/Card";

const Hand = ({ player, handleClickSelect }) => {
  return player.nameOfPlayer === "player1" ? (
    <div className="PlayerOneWrapper">
      <Card player={player} handleClickSelect={handleClickSelect} />
    </div>
  ) : (
    <div className="PlayerTwoWrapper">
      <Card player={player} handleClickSelect={handleClickSelect} />
    </div>
  );
};

export default Hand;
